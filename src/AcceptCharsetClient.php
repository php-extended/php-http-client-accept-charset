<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-charset library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Charset\ISO_8859_1;
use PhpExtended\Charset\UTF_8;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * AcceptCharsetClient class file.
 * 
 * This class is an implementation of a client which adds accept-charset headers
 * on incoming requests.
 * 
 * @author Anastaszor
 */
class AcceptCharsetClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The configuration.
	 * 
	 * @var AcceptCharsetConfiguration
	 */
	protected AcceptCharsetConfiguration $_configuration;
	
	/**
	 * Builds a new AcceptCharsetClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param AcceptCharsetConfiguration $configuration
	 */
	public function __construct(ClientInterface $client, ?AcceptCharsetConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new AcceptCharsetConfiguration();
		}
		
		$this->_configuration = $configuration;
		if($this->_configuration->getAcceptChain()->isEmpty())
		{
			try
			{
				// apply default us configuration
				$this->_configuration->addValue(UTF_8::class);
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to do
			}
			
			try
			{
				// TODO destination domain based detection and choice
				$this->_configuration->addValue(ISO_8859_1::class, 0.5);
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to do
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Accept-Charset') && !$this->_configuration->getAcceptChain()->isEmpty())
		{
			try
			{
				$request = $request->withHeader('Accept-Charset', $this->_configuration->getAcceptChain()->getHeaderValue());
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to do
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
