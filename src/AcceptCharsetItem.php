<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-charset library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Charset\CharacterSetInterface;
use Stringable;

/**
 * AcceptCharsetItem class file.
 * 
 * This class represents an item for the accept charset chain.
 * 
 * @author Anastaszor
 */
class AcceptCharsetItem implements Stringable
{
	
	/**
	 * The charset.
	 * 
	 * @var CharacterSetInterface
	 */
	protected CharacterSetInterface $_charset;
	
	/**
	 * The q-value.
	 * 
	 * @var float
	 */
	protected float $_qvalue = 1.0;
	
	/**
	 * Builds a new AcceptCharsetItem with the given character set and q-value.
	 * 
	 * @param CharacterSetInterface $charset
	 * @param float $qvalue
	 */
	public function __construct(CharacterSetInterface $charset, float $qvalue = 1.0)
	{
		$this->_charset = $charset;
		$this->_qvalue = $qvalue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the charset.
	 * 
	 * @return CharacterSetInterface
	 */
	public function getCharset() : CharacterSetInterface
	{
		return $this->_charset;
	}
	
	/**
	 * The q-value of this item.
	 * 
	 * @return float
	 */
	public function getQValue() : float
	{
		return $this->_qvalue;
	}
	
	/**
	 * Gets the header value of this accept charset item.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		if(1.0 === $this->getQValue())
		{
			return $this->getCharset()->getName();
		}
		
		return $this->getCharset()->getName().';q='.((string) $this->getQValue());
	}
	
}
