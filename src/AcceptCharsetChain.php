<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-charset library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use Stringable;

/**
 * AcceptCharsetChain class file.
 * 
 * This class represents the chain of accept charset items.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<int, AcceptCharsetItem>
 */
class AcceptCharsetChain implements Countable, IteratorAggregate, Stringable
{
	
	/**
	 * The items.
	 * 
	 * @var array<integer, AcceptCharsetItem>
	 */
	protected array $_items = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds an item to the chain.
	 * 
	 * @param AcceptCharsetItem $item
	 */
	public function addItem(AcceptCharsetItem $item) : void
	{
		$this->_items[] = $item;
	}
	
	/**
	 * Gets the header value of the chain.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		return \implode(',', \array_map(function(AcceptCharsetItem $item)
		{
			return $item->getHeaderValue();
		}, $this->_items));
	}
	
	/**
	 * Gets whether this chain is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 * @return Iterator<AcceptCharsetItem>
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_items);
	}
	
}
