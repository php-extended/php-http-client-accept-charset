<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-charset library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Charset\CharacterSetReference;
use PhpExtended\Charset\CharacterSetReferenceInterface;
use Stringable;

/**
 * AcceptCharsetConfiguration class file.
 * 
 * This class stores the configuration for the accept-charset client.
 * 
 * @author Anastaszor
 */
class AcceptCharsetConfiguration implements Stringable
{
	
	/**
	 * The accept chain.
	 * 
	 * @var ?AcceptCharsetChain
	 */
	protected ?AcceptCharsetChain $_acceptChain = null;
	
	/**
	 * The charset reference, to find the charsets by name.
	 * 
	 * @var ?CharacterSetReferenceInterface
	 */
	protected ?CharacterSetReferenceInterface $_charsetReference = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds the given character set with the given q-value to the chain.
	 * 
	 * @param string $charset
	 * @param float $qvalue
	 * @throws InvalidArgumentException if the charset does not exists
	 */
	public function addValue(string $charset, float $qvalue = 1.0) : void
	{
		$charset = $this->getCharsetReference()->lookupAlias($charset);
		if(null === $charset)
		{
			throw new InvalidArgumentException(\strtr('The character set {charset} was not found.', ['{charset}' => $charset]));
		}
		
		$this->getAcceptChain()->addItem(new AcceptCharsetItem($charset, $qvalue));
	}
	
	/**
	 * Gets the accept chain.
	 * 
	 * @return AcceptCharsetChain
	 */
	public function getAcceptChain() : AcceptCharsetChain
	{
		if(null === $this->_acceptChain)
		{
			$this->_acceptChain = new AcceptCharsetChain();
		}
		
		return $this->_acceptChain;
	}
	
	/**
	 * Gets the character set reference.
	 * 
	 * @return CharacterSetReferenceInterface
	 */
	protected function getCharsetReference() : CharacterSetReferenceInterface
	{
		if(null === $this->_charsetReference)
		{
			$this->_charsetReference = new CharacterSetReference();
		}
		
		return $this->_charsetReference;
	}
	
}
