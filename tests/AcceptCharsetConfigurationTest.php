<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-charset library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptCharsetConfiguration;
use PHPUnit\Framework\TestCase;

/**
 * AcceptCharsetConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptCharsetConfiguration
 *
 * @internal
 *
 * @small
 */
class AcceptCharsetConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptCharsetConfiguration
	 */
	protected AcceptCharsetConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptCharsetConfiguration();
	}
	
}
